﻿namespace Vehicles.Actions
{
    interface IRider
    {
        void Ride();
    }
}
