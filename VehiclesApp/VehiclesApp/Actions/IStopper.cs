﻿namespace Vehicles.Actions
{
    interface IStopper
    {
        void StopEngine();
    }
}
