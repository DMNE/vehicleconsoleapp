﻿namespace Vehicles.Actions
{
    interface IFlyer
    {
        void Fly();
    }
}
