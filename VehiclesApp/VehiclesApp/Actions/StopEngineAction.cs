﻿using System;

namespace Vehicles.Actions
{
    class StopEngineAction : IAction
    {
        public void DoAction()
        {
            Console.WriteLine("Stop!");
        }
    }
}
