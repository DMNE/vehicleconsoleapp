﻿namespace Vehicles.Actions
{
    interface IAction
    {
        void DoAction();
    }
}
