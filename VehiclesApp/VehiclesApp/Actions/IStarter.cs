﻿namespace Vehicles.Actions
{
    interface IStarter
    {
        void StartEngine();
    }
}
