﻿using System;

namespace Vehicles.Actions
{
    class FlyAction : IAction
    {
        public void DoAction()
        {
            Console.WriteLine("Fly");
        }
    }
}
