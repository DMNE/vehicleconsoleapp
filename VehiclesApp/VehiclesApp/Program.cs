﻿using System;
using Vehicles.VehicleTypes;
using Vehicles.VehicleTypes.CarTypes;

namespace Vehicles
{
    class Program
    {
        static bool work = true;
        static string chooser;

        static void Main()
        {
            Console.WriteLine("Welcome to my Vehicle program.");

            Chooser();

        }

        public static void Chooser()
        {

            while (work)
            {
                Console.WriteLine("Please chose your vehicle:\n" +
                                        "\tAirplane - 0\n" +
                                        "\tBoat - 1\n" +
                                        "\tBus - 2\n" +
                                        "\tSedan - 3\n" +
                                        "\tTruck - 4\n" +
                                        "or enter samething else for exit.");

                chooser = Console.ReadLine();

                switch (chooser)
                {
                    case "0":
                        Airplane airplane = new Airplane();
                        airplane.Model = "Boeng";
                        Console.WriteLine(airplane.Model);
                        airplane.StartEngine();
                        airplane.Ride();
                        airplane.Fly();
                        airplane.Ride();
                        airplane.StopEngine();
                        Chooser();
                        break;

                    case "1":
                        Boat boat = new Boat();
                        boat.Model = "Нырок";
                        Console.WriteLine(boat.Model);
                        boat.StartEngine();
                        boat.Swim();
                        boat.StopEngine();
                        Chooser();
                        break;

                    case "2":
                        Bus bus = new Bus();
                        bus.Model = "Icarus";
                        Console.WriteLine(bus.Model);
                        bus.StartEngine();
                        bus.Ride();
                        bus.InPassanger();
                        bus.Ride();
                        bus.OutPassanger();
                        bus.Ride();
                        bus.StopEngine();
                        Chooser();
                        break;

                    case "3":
                        Car car = new Car();
                        car.Model = "Суперкар";
                        Console.WriteLine(car.Model);
                        car.StartEngine();
                        car.Ride();
                        car.StopEngine();
                        Chooser();
                        break;

                    case "4":
                        Truck truck = new Truck();
                        truck.Model = "MAN";
                        Console.WriteLine(truck.Model);
                        truck.StartEngine();
                        truck.Ride();
                        truck.StopEngine();
                        truck.Load();
                        truck.StartEngine();
                        truck.Ride();
                        truck.StopEngine();
                        truck.Unload();
                        Chooser();
                        break;

                    default:
                        Console.WriteLine("Your enter for Exit.");
                        work = false;
                        break;
                }
                return;
            }
        }
    }
}

