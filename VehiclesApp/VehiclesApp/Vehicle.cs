﻿using System;
using Vehicles.Actions;

namespace Vehicles
{
    class Vehicle : IStarter, IStopper
    {
        public string Model { get; set; }

        IAction startEngine;
        IAction stopEngine;

        public Vehicle()
        {
            startEngine = new StartEngineAction();
            stopEngine = new StopEngineAction();
        }
        public virtual void StartEngine()
        {
            startEngine.DoAction();
        }

        public void StopEngine()
        {
            stopEngine.DoAction();
        }

        public virtual void Beep()
        {
            Console.WriteLine("BEEEEEEEeeeP!");
        }
    }
}
