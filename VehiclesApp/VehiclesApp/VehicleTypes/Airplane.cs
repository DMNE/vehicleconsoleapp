﻿using System;
using Vehicles.Actions;

namespace Vehicles.VehicleTypes
{
    class Airplane : Vehicle, IRider, IFlyer
    {
        IAction rideAction;
        IAction flyAction;

        public Airplane()
        {
            rideAction = new RideAction();
            flyAction = new FlyAction();
        }

        public void Ride()
        {
            rideAction.DoAction();
        }

        public void Fly()
        {
            flyAction.DoAction();
        }

    }
}
