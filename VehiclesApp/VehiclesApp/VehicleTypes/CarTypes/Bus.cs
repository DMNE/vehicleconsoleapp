﻿using System;

namespace Vehicles.VehicleTypes.CarTypes
{
    class Bus : Car
    {
        public void InPassanger()
        {
            Console.WriteLine("Passanger in.");
        }

        public void OutPassanger()
        {
            Console.WriteLine("Passanger out.");
        }
    }
}
