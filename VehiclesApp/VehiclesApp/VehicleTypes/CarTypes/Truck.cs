﻿using System;

namespace Vehicles.VehicleTypes.CarTypes
{
    class Truck : Car
    {
        public override void Beep()
        {
            Console.WriteLine("TUUU-TUUU!");
        }

        public void Load()
        {
            Console.WriteLine("Cargo load.");
        }

        public void Unload()
        {
            Console.WriteLine("Cargo unload.");
        }
    }
}
