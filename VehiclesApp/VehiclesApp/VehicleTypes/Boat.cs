﻿using System;
using Vehicles.Actions;

namespace Vehicles.VehicleTypes
{
    class Boat : Vehicle, ISwimmer
    {
        IAction swimAction;

        public Boat()
        {
            swimAction = new SwimAction();
        }

        public void Swim()
        {
            swimAction.DoAction();
        }
    }
}
