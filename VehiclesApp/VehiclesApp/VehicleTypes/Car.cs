﻿using System;
using Vehicles.Actions;

namespace Vehicles.VehicleTypes
{
    class Car : Vehicle, IRider
    {
        IAction rideAction;

        public Car()
        {
            rideAction = new RideAction();
        }

        public void Ride()
        {
            rideAction.DoAction();
        }

    }
}
